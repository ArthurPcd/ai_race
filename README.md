# AI_Race
Create an AI that can race as a human in a pre-made env (GYM).
## Description
Easiest continuous control task to learn from pixels, a top-down
racing environment. Discreet control is reasonable in this environment as
well, on/off discretisation is fine.
The game is solved when the agent consistently gets 900+ points.
The generated track is random every episode.
Some indicators are shown at the bottom of the window along with the
state RGB buffer. From left to right: true speed, four ABS sensors,
steering wheel position, gyroscope.
## Observation Space
State consists of 96x96 pixels.
## Rewards
The reward is -0.1 every frame and +1000/N for every track tile visited,
where N is the total number of tiles visited in the track. For example,
if you have finished in 732 frames, your reward is
1000 - 0.1*732 = 926.8 points.
## Starting State
The car starts stopped at the center of the road.
## Episode Termination
The episode finishes when all the tiles are visited. The car also can go
outside of the playfield - that is far off the track, then it will
get -100 and die.
## Arguments
There are no arguments supported in constructing the environment.
## Version History
- v0: Current version
***

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ArthurPcd/ai_race.git
git branch -M main
git push -uf origin main
```

## Authors and acknowledgment
Arthur Pacaud Epitech Student<br />
Thomas Joan Epitech Student<br />
